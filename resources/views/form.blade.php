<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Tugas Day 1</title>
    </head>
    <body>
        
        <h1>Buat Account Baru!</h1>

        <h2>Sign Up Form</h2>

        <form method="POST" action="{{ route('welcome') }}">
        {{ csrf_field() }}
            <label>First name:</label><br><br>
            <input type="text" name="firstname" required><br><br>

            <label>Last name:</label><br><br>
            <input type="text" name="lastname" required><br><br>

            <label>Gender:</label><br><br>
            <input type="radio" name="gender" value="Male">Male<br>
            <input type="radio" name="gender" value="Female">Female<br>
            <input type="radio" name="gender" value="Other">Other<br><br>

            <label>Nationality</label><br><br>
            <select name="nationality">
                <option value="Indonesian">Indonesian</option>
                <option value="Singapore">Singapore</option>
                <option value="Malaysia">Malaysia</option>
                <option value="Australian">Australian</option>
            </select><br><br>

            <label>Language Spoken</label><br><br>
            <input type="checkbox">Bahasa Indonesia<br>
            <input type="checkbox">English<br>
            <input type="checkbox">Other
            <br><br>
            
            <label>Bio:</label><br><br>
            <textarea name="textarea" cols="30" rows="10"></textarea><br>

            <input type="submit" value="Sign Up">

        </form>

    </body>
</html>