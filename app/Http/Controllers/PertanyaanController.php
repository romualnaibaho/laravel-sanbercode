<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PertanyaanController extends Controller
{
    public function create(){
        return view('question.create');
    }

    public function index(){
        $pertanyaan = DB::table('pertanyaan')->get();
        return view('question.list', compact('pertanyaan'));
    }

    public function store(Request $request){
        DB::table('pertanyaan')->insert([
			'judul' => $request->judul,
			'isi' => $request->isi
        ]);
        
        return redirect('/pertanyaan');
    }

    public function show($pertanyaan_id){
        $pertanyaan = DB::table('pertanyaan')->where('id',$pertanyaan_id)->get();
        
        return view('question.show', compact('pertanyaan'));
    }

    public function edit($pertanyaan_id){
        $pertanyaan = DB::table('pertanyaan')->where('id',$pertanyaan_id)->get();
        
        return view('question.edit', compact('pertanyaan'));
    }

    public function update($pertanyaan_id, Request $request){
        DB::table('pertanyaan')->where('id',$pertanyaan_id)->update([
			'judul' => $request->judul,
			'isi' => $request->isi
        ]);
        
        return redirect('/pertanyaan');
    }

    public function destroy($pertanyaan_id){
        DB::table('pertanyaan')->where('id',$pertanyaan_id)->delete();
        
        return redirect('/pertanyaan');
    }
}
