<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class homeController extends Controller
{
    public function viewHome(){
        return view('home');
    }

    public function register(){
        return view('form');
    }

    public function welcome(){
        return view('welcome');
    }

    public function viewWelcome(Request $request){
        $fnama = $request->input('firstname');
        $lnama = $request->input('lastname');

        $fullname = $fnama . " " . $lnama;

        return view('welcome')->with("fullname", $fullname);
    }
}
